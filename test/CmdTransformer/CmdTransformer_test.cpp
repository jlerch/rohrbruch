#include <catch2/catch.hpp>
#include <iostream>
#include <opencv4/opencv2/imgcodecs.hpp>

#include "CmdTransformer.hpp"

TEST_CASE("convert int number to hex digit", "[]")
{
    // unsigned int num = 127;
    // char hex_string[2];
    // CmdTransformer::int_to_hex(hex_string, num);
    // REQUIRE(hex_string[0] == '7');
    // REQUIRE(hex_string[1] == 'f');
}

TEST_CASE("convert opencv Matrix into 2d hex array")
{
    std::string IMG_PATH = "../img/small_tree.jpg";
    cv::Mat img_mat_cv = cv::imread(IMG_PATH, cv::ImreadModes::IMREAD_COLOR);

    int img_cols = img_mat_cv.cols;
    int img_rows = img_mat_cv.rows;
    char img_mat_hex[img_cols * img_rows * PXL_VAL_LEN];

    // CmdTransformer::cv_mat_to_hex_array(img_mat_hex, img_mat_cv, img_cols,
    //                                    img_rows);
}