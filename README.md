```
,---.    .---.  .-. .-.,---.    ,---.   ,---.  .-. .-.  ,--,  .-. .-. 
| .-.\  / .-. ) | | | || .-.\   | .-.\  | .-.\ | | | |.' .')  | | | | 
| `-'/  | | |(_)| `-' || `-'/   | |-' \ | `-'/ | | | ||  |(_) | `-' | 
|   (   | | | | | .-. ||   (    | |--. \|   (  | | | |\  \    | .-. | 
| |\ \  \ `-' / | | |)|| |\ \   | |`-' /| |\ \ | `-')| \  `-. | | |)| 
|_| \)\  )---'  /(  (_)|_| \)\  /( `--' |_| \)\`---(_)  \____\/(  (_) 
    (__)(_)    (__)        (__)(__)         (__)             (__)     
```

[Pixelflut](https://hackaday.com/2020/08/01/playing-the-pixelflut/) client written in Cpp.

## Installation
The build system used is [meson](https://mesonbuild.com/) in combination with [ninja](https://ninja-build.org/).
1. install dependencies:
```
catch2 boost opencv2
```
2. build the software
```
meson build
ninja -C build
```

For a more detailed explanation please look up meson's and ninja's documentations.

## Usage
Type
```
cd build
./rohrbruch --help
```
to see the usage information.
