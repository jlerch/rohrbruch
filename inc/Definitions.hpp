#define PXL_VAL_LEN                                                            \
    6 // length of the hex-number that represents the pixel value

#define PXLFLT_CMD_MAX_LEN                                                     \
    (PXL_VAL_LEN + 14) // max lengt of the command PX <x> <y> <hex>