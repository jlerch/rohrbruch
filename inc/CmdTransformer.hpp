#include <cassert>
#include <opencv4/opencv2/imgcodecs.hpp>
#include <random>
#include <stdexcept>

#include "Definitions.hpp"

class CmdTransformer
{
  public:
    /**
     * @brief convert opencv image matrix into a string of pixelflut-commands.
     * The commands are shuffled. The output string will be seperated into equal
     * sized segments of length PXLFLT_CMD_MAX_LEN. If a command string is not
     * as long as PXLFLT_CMD_MAX_LEN, it is filled up with space padding.
     *
     * @param[out] cmd_str empty string to write the output of the function
     * into. Holds multiple commands written after another.
     * @param[in] img_mat_cv cv::Mat object
     * @param[in] offset_x
     * @param[in] offset_y
     * @param[in] canvas_width
     * @param[in] canvas_height
     */
    static void cv_mat_to_cmd_string(std::string& cmd_str,
                                     const cv::Mat& img_mat_cv,
                                     const int offset_x, const int offset_y,
                                     const int canvas_width,
                                     const int canvas_height);

  private:
    /**
     * @brief Convert a number (range 0 - 255) into a 2-character string of
     * hex digits
     *
     * @param[out] hex_string 2 character big char array
     * @param[in] num
     * @return char* 2 character long char array
     */
    static void int_to_hex(char* const hex_string, const unsigned int num);

    /**
     * @brief Convert cv::Mat to vector of pixelflut commands
     *
     * @param[out] cmd_vector
     * @param[in] img_mat_cv
     * @param[in] offset_x
     * @param[in] offset_y
     * @param[in] canvas_width
     * @param[in] canvas_height
     */
    static void cv_mat_to_cmd_vector(std::vector<std::string>& cmd_vector,
                                     const cv::Mat& img_mat_cv,
                                     const int offset_x, const int offset_y,
                                     const int canvas_width,
                                     const int canvas_height);

    /**
     * @brief Convert string-vector into one long string
     *
     * @param[out] cmd_str
     * @param[in] cmd_vec
     */
    static void
    cmd_vector_to_cmd_string(std::string& cmd_str,
                             const std::vector<std::string>& cmd_vec);
};