#include "CmdTransformer.hpp"

// ===== PUBLIC ===== //

void CmdTransformer::cv_mat_to_cmd_string(
    std::string& cmd_str, const cv::Mat& img_mat_cv, const int offset_x,
    const int offset_y, const int canvas_width, const int canvas_height)
{
    cmd_str = "";
    std::vector<std::string> cmd_vec;
    cv_mat_to_cmd_vector(cmd_vec, img_mat_cv, offset_x, offset_y, canvas_width,
                         canvas_height);

    // shuffle vector
    unsigned seed = 42;
    std::shuffle(cmd_vec.begin(), cmd_vec.end(),
                 std::default_random_engine(seed));

    cmd_vector_to_cmd_string(cmd_str, cmd_vec);
}

// ===== PRIVATE ===== //

void CmdTransformer::cv_mat_to_cmd_vector(std::vector<std::string>& cmd_vector,
                                          const cv::Mat& img_mat_cv,
                                          const int offset_x,
                                          const int offset_y,
                                          const int canvas_width,
                                          const int canvas_height)
{
    assert(!cmd_vector.empty());

    for (int y = 0; y < img_mat_cv.rows; ++y)
        {
            for (int x = 0; x < img_mat_cv.cols; ++x)
                {
                    // get color value
                    char hex_blue[2];
                    char hex_green[2];
                    char hex_red[2];
                    char hex_alpha[2];

                    int dec_blue =
                        cv::Scalar(img_mat_cv.at<cv::Vec3b>(y, x))[0];
                    int dec_green =
                        cv::Scalar(img_mat_cv.at<cv::Vec3b>(y, x))[1];
                    int dec_red = cv::Scalar(img_mat_cv.at<cv::Vec3b>(y, x))[2];
                    int dec_alpha =
                        cv::Scalar(img_mat_cv.at<cv::Vec3b>(y, x))[3];

                    // only send pixels that are not transparent
                    if (dec_alpha != 255)
                        {
                            int_to_hex(hex_blue, dec_blue);
                            int_to_hex(hex_green, dec_green);
                            int_to_hex(hex_red, dec_red);

                            // create command string
                            // ..cmd holds exacly one command
                            std::string cmd =
                                "PX " +
                                std::to_string((x + offset_x) % canvas_width) +
                                " " +
                                std::to_string((y + offset_y) % canvas_height) +
                                " " + hex_red[0] + hex_red[1] + hex_green[0] +
                                hex_green[1] + hex_blue[0] + hex_blue[1] + "\n";

                            cmd_vector.push_back(cmd);
                        }
                }
        }
}

void CmdTransformer::cmd_vector_to_cmd_string(
    std::string& cmd_str, const std::vector<std::string>& cmd_vec)
{
    cmd_str = "";
    for (int i = 0; i < cmd_vec.size(); ++i)
        {
            cmd_str += cmd_vec.at(i);
        }
}

void CmdTransformer::int_to_hex(char* const hex_string, const unsigned int num)
{
    if (num > 255)
        {
            throw std::runtime_error("int_to_hex: 'num' is bigger than 255.");
        }

    // create lookup-table: int -> hex for one hex-digit
    char int_to_hex_table[16] = {
        '0', '1', '2', '3', '4', '5', '6', '7',
        '8', '9', 'a', 'b', 'c', 'd', 'e', 'f',
    };

    // get first hex digit
    int first_digit = num >> 4;
    hex_string[0] = int_to_hex_table[first_digit];

    // git latter hex digit
    int latter_digit = num % 0b10000;
    hex_string[1] = int_to_hex_table[latter_digit];
}