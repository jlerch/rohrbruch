#include <boost/asio.hpp>
#include <boost/program_options.hpp>
#include <iostream>
#include <opencv4/opencv2/imgcodecs.hpp>
#include <stdexcept>
#include <string_view>
#include <thread>

#include "CmdTransformer.hpp"

#include "Definitions.hpp"

namespace as = boost::asio;
namespace po = boost::program_options;

void handler(const boost::system::error_code& e, std::size_t size);

int main(int argc, char const* argv[])
{
    boost::system::error_code error;

    std::string img_path;

    int offset_x;
    int offset_y;

    std::string canvas_size;
    int canvas_width;
    int canvas_height;

    bool alpha;

    int port;
    std::string host;

    // handle command line options
    // clang-format off
    po::options_description desc("Allowed options");
    desc.add_options()
        ("help",
                "print help message")
        ("offset-x,x", po::value<int>(&offset_x)->default_value(0),
                "set x offset of the picture's origin")
        ("offset-y,y", po::value<int>(&offset_y)->default_value(0),
                "set y offset of the picture's origin")
        ("host,h", po::value<std::string>(&host),
                "IP-adress of the pixelflut server")
        ("port,p", po::value<int>(&port)->default_value(1234),
                "port of the pixelflut server")
        ("image-path,i", po::value<std::string>(&img_path),
                "path to the image you want to send")
        ("alpha,a", po::value<bool>(&alpha)->default_value(false),
                "[TODO] decide if you want to use send alpha channel of a picture")
    ;
    // clang-format on

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help"))
        {
            std::cout << desc << "\n";
            return 1;
        }

    // open connection
    as::io_context io_context;
    as::ip::tcp::socket socket(io_context);
    as::ip::address host_addr = as::ip::make_address(host);
    as::ip::tcp::endpoint endpoint_server(host_addr, port);

    socket.connect(endpoint_server);

    std::cout << "Connection established with: " << endpoint_server.address()
              << " " << endpoint_server.port() << std::endl;

    // ask for canvas size
    as::async_read_until(socket, as::dynamic_buffer(canvas_size), '\n',
                         handler);
    as::async_write(socket, as::buffer("SIZE\n"), handler);
    if (error)
        {
            throw boost::system::system_error(error);
        }
    io_context.run(); // wait for both handlers to return
    std::cout << "server size: " << canvas_size << std::endl;
    sscanf(canvas_size.c_str(), "%*s %i %i\n", &canvas_width, &canvas_height);

    // read and convert image to pxlflt command string
    cv::Mat img_mat_cv = cv::imread(img_path, cv::ImreadModes::IMREAD_COLOR);
    if (img_mat_cv.data == NULL)
        {
            std::cerr << "Image could not be read." << std::endl;
            return 1;
        }

    std::string cmd_str("");
    CmdTransformer::cv_mat_to_cmd_string(cmd_str, img_mat_cv, offset_x,
                                         offset_y, canvas_width, canvas_height);
    std::string_view cmd_str_view(cmd_str);

    // write pixels to host
    for (;;)
        {
            as::write(socket, as::buffer(cmd_str_view), error);
            if (error)
                {
                    throw boost::system::system_error(error);
                }
        }
    return 0;
}

void handler(const boost::system::error_code& e, std::size_t size)
{
    if (e)
        {
            throw boost::system::system_error(e);
        }
}